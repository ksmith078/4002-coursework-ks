from flask import Flask, render_template, url_for, request, session, redirect
from flask_pymongo import PyMongo
import bcrypt
import os

app = Flask(__name__)
#client = MongoClient(os.environ['MONGODB_HOST'], 27017)
#mongo = PyMongo(app)
#db = client.bbgdb

@app.route('/')
def home():
    if 'username' in session:
        return 'You are logged in as ' + session['username']
    return render_template("index.html", appName = "Big Games")

@app.route('/games')
def games():
    return render_template("games.html", appName = "Big Games")

@app.route('/games/blackjack')
def blackjack():
    return render_template("blackjack.html", appName = "Big Games")

@app.route('/login')
def login():
    return render_template("login.html", appName = "Big games")

@app.route('/signup', methods=['POST', 'GET'])
def signup():
    if request.method == 'POST':
        users = mongo.db.users
        existing_user = users.find_one({'name' : request.form['username']})
        if existing_user is None:
            hashpass = bcrypt.hashpw(request.form['pass'].encode('utf-8'), bcrypt.genSalt())
            users.inert({'name' : request.form['username'], password : hashpass})
            session['username'] = request.form['username']
            return redirect(url_for('home'))
        return 'Username already exists'
    return render_template("signup.html", appName = "Big Games")

@ app.route('/about')
def about():
    return render_template("about.html", appName = "Big Games")

app.run(host="0.0.0.0", debug=True)
