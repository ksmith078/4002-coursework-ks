var play = 'True';
var logPos = 'False';
var bet = 0;
var bust = 'False';
var lockBet = 'False';
var bank = '1000';
var resetText = 'False';
var stick = 'False';
var dealerHitMe = 'False';
var dealerStick = '';
var playerWin = '';
var dealerSum = 0;
var dealerBust = 'False';
var chips = [
   {
   value: 1,
   colour: 'white',
  },
  {
    value: 10,
    colour: 'red',
  },
  {
    value: 50,
    colour: 'green',
  },
  {
    value: 100,
    colour: 'black',
  },
  {
    value: 500,
    colour: 'purple',
  },
];
//Creates each individual card
function card(value, name, suit){
	this.value = value;
	this.name = name;
	this.suit = suit;
}

//Creates the deck
function deck(){
	this.names = ['A', '2', '3', '4', '5', '6', '7', '8', '9', '10', 'Jack', 'Queen', 'King'];
	this.suits = ['Hearts','Diamonds','Spades','Clubs'];
	var cards = [];

    for( var s = 0; s < this.suits.length; s++ ) {
        for( var n = 0; n < this.names.length; n++ ) {
            cards.push( new card( n+1, this.names[n], this.suits[s] ) );
        }
    }

    return cards;
}

//Shuffles the deck
function cardShuffle(o) {
	for(var j, x, i = o.length; i; j = parseInt(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
    console.log("deck shuffled")
	return o;
}

//Re value the deck so that J,Q,K = 10 rather than 11,12,13
function reValue(deck) {
    for (var i in deck) {
       if (deck[i].value > 10) {
         deck[i].value = 10 ;
      }
  }
  console.log("deck revalued for blackjack");
  return deck;
}


//draws a card for either the player or dealer
function drawOne(deck, thisHand) {

  thisHand.push(deck[cardCount])
  cardCount++
  console.log("card count: " + cardCount)
  return thisHand

}

function valueSum(thisHand, valueSum) {
  valueSum = 0
  for (var i in thisHand) {
    valueSum  = valueSum + thisHand[i].value
    i++
  }
  console.log("value sum: " + valueSum)
  return valueSum

}

function cardFill(card) { //this function associates a colour with the suits and their symbol for display
  var cardFill = '';
  var symbol = '';
  if (card.suit == 'Hearts') {
    cardFill = 'red';
  //  console.log('Card fill is red!');
    symbol = '♥';
    //console.log(card);
    return [cardFill, symbol];
  } else if (card.suit == 'Spades'){
    cardFill = 'black';
    //console.log('Card fill is black!');
    symbol = '♠';
//   console.log(card);
    return [cardFill, symbol];
  } else if (card.suit == 'Diamonds'){
    cardFill = 'red';
  //  console.log('Card fill is red!');
    symbol = '♦';
  //  console.log(card);
    return [cardFill, symbol];
  } else if (card.suit == 'Clubs'){
    cardFill = 'black';
  //  console.log('Card fill is black!');
    symbol = '♣';
 //   console.log(card);
    return [cardFill, symbol];
  }

}

var myDeck = new deck();
myDeck = cardShuffle(myDeck);
var newDeck = reValue(myDeck);
console.log(newDeck);
var cardCount = 0;
var hand = [];
var dealerHand = [];
var sum = 0;

function hitMe() {
  resetText = 'True';
	if (bust == 'False' && stick == 'False'){
		console.log('Hit me!');
		drawOne(newDeck, hand);
		console.log(hand);
		sum = valueSum(hand, sum);
		if (sum > 21) {
	      bust = 'True';
				console.log("You're bust!");
	    }
	}
}

function stickMe() {
  console.log('Stick me!');
  if (bust == 'False') {
    stick = 'True';
    resetText = 'True';
    dealerPlay();
  }

}

function dealerPlay() {
  console.log('dealerPlay')
  dealerSum = 0;
  for (var i in dealerHand) { // sum up the dealer hand
      dealerSum = dealerSum + dealerHand[i].value;
    }
  if (dealerSum < 16) { // if the dealer wants another card
    console.log('dealerSum < 16')
    dealerHitMe = 'True';
    dealerStick = 'False';
    drawOne(newDeck, dealerHand);
    dealerSum = 0;
    for (i in dealerHand) { // sum up the dealer hand
      dealerSum = dealerSum + dealerHand[i].value;
    }
    console.log('dealer hand: ');
    console.log(dealerHand);
    console.log('new dealer sum: ' + dealerSum);
    if (dealerSum < 16) { // if the dealer wants another card
      dealerPlay(); // call the function again to get another card
    } else { //if the dealer doesnt want another card, he is sticking
      console.log('dealer sticking at: ' + dealerSum +'#c');
      dealerStick = 'True';
    }
  }
  if (dealerSum > 21) { // is dealer bust?
      console.log('dealer is bust!');
      dealerHitMe = 'False';
      dealerBust = 'True';
      playerWin = 'True';
    }
  if (dealerSum > sum && dealerBust == 'False') {
    dealerStick = 'True';
    console.log('dealer sticking at: ' + dealerSum + '#a');
    console.log('dealer has won! all ur monies belong to me');
    playerWin = 'False';
    }
  if (dealerSum == sum) { // is it a tie?, what happens in this instance
    console.log('Value sums match! (tie)');
  }
  if (sum > dealerSum) {
    console.log('player has won! #b');
    playerWin = 'True';
  }
}

var cardsDrawn = 2; //your hand starts with 2 cards drawn on the canvas
var newCardXPos = 50; //variable to control position of new cards in your hand
var dealerNewCardXPos = 230;
var dealerCardsDrawn = 2;

function setup() {
  createCanvas(400, 400);
  background('darkGreen');

    //Bet, bank and sum text
  fill('black');
  textSize(16);
  text('Bank: £' + bank, 10, 20);
  if (lockBet == 'True') {
    fill('yellow');
  }
  text('Bet: £' + bet, 300, 20);
  textSize(24);
  fill('black');

  text(sum, 60, 180);

    //draws the chips
  var x = 50;
  var y = 350;
  var w = 50;
  var h = 50;
  for (var i in chips) {
    fill(chips[i].colour);
    strokeWeight(2);
    ellipse(x, y, w, h);
    fill('lightBlue');
    textSize(16);
    text(chips[i].value, x-10, y);
    x = x + 75;
  }
  strokeWeight(2);
  rect(50, 200, 70, 100); //These are your cards
  rect(130, 200, 70, 100);
  var symbol = ''
  var cardData = cardFill(hand[0]);
  //console.log(cardData)
  fill(cardData[0])
  textSize(14);
  text(hand[0].name + cardData[1], 60, 220);

	//Is card black or red? Display the 'name' in appropriate colour
  cardData = cardFill(hand[1]);
  fill(cardData[0])
  text(hand[1].name + cardData[1], 140, 220);

  fill('coral'); //Dealers cards, one face up
  strokeWeight(2);
  rect(190, 40, 70, 100);
  fill('lightBlue');
  rect(230, 45, 70, 100);
  cardData = cardFill(dealerHand[1]);
  fill(cardData[0]);
  textSize(14);
  text(dealerHand[1].name + cardData[1], 240, 65);

}

function draw() {

  if (cardsDrawn != Object.keys(hand).length) { //if the hand variable is bigger than however many cards we have drawn to the canvas, then we need to draw the new card
    //console.log(Object.keys(hand).length)
    stroke(0,0,0);
    strokeWeight(2);
    fill('lightBlue')
    rect(newCardXPos, 225, 70, 100);
    cardData = cardFill(hand[cardsDrawn]);
    fill(cardData[0]);
    noStroke();
    textSize(14);
    text(hand[cardsDrawn].name + cardData[1], newCardXPos+10, 240);
    newCardXPos = newCardXPos + 25;
    cardsDrawn++;
  }
  if (dealerCardsDrawn != Object.keys(dealerHand).length) {
    stroke(0,0,0);
    strokeWeight(2);
    fill('lightBlue');
    rect(dealerNewCardXPos, 70, 70, 100);
    cardData = cardFill(dealerHand[dealerCardsDrawn]);
    console.log(cardData);
    fill(cardData[0]);
    noStroke();
    textSize(14);
    text(dealerHand[dealerCardsDrawn].name + cardData[1], dealerNewCardXPos+10, 95);
    dealerNewCardXPos = dealerNewCardXPos + 25;
    dealerCardsDrawn++;
  }

  if (resetText == 'True'){
    noStroke();
    fill('darkGreen');
    rect(0, 0, 400, 40);
    rect(58, 144, 50, 50);
    resetText = 'False';
    console.log('Text reset!');

        //Bet, bank and sum text
    fill('black');
    textSize(16);
    text('Bank: £' + bank, 10, 20);
    if (lockBet == 'True') {
      fill('yellow');
    }
    if (bust == 'True') {
      fill('red');
    }
    text('Bet: £' + bet, 300, 20);
    textSize(24);
    fill('black');
    if (stick == 'True') {
      fill('yellow');
  }
    text(sum, 60, 180);
  }

if (bust == 'True' && play == 'True') {
	textSize(50);
	fill('red');
	stroke('black');
	strokeWeight(5);
	text('BUST!', 200, 200);
    bet = 0;
    resetText = 'True';
    play = 'False';
}

  if (lockBet == 'False') { //handles position and button functions
    var placeButton = createButton('Place bet!');
    placeButton.mousePressed(placeBet);
    placeButton.position(300,300) ;
  } else if (lockBet == 'True' && bust == 'False') {
    var hitButton = createButton('Hit me!'); //for the buttons
    hitButton.position(250, 270);
    var stickButton = createButton('Stick!');
    stickButton.position(330, 270);
    hitButton.mousePressed(hitMe);
    stickButton.mousePressed(stickMe);
  }
  if (logPos == 'True') {
    console.log(mouseX);
    console.log(mouseY);
  }
  if (playerWin == 'False' && play == 'True') {
    textSize(24);
    fill('yellow');
    text(dealerSum,140 ,70);
    textSize(50);
    fill('red');
    stroke(0,0,0);
    strokeWeight(2);
    text('LOSS!', 200, 200);
    bet = 0;
    resetText = 'True';
    play = 'False';
  }
  if (playerWin == 'True'&& play == 'True') {
    textSize(24);
    fill('yellow');
    text(dealerSum,140 ,70);
    textSize(50);
    fill('green');
    stroke(0,0,0);
    strokeWeight(2);
    text('WIN!', 200, 200);
    bank = bank + (bet * 2);
    bet = 0;
    resetText = 'True';
    play = 'False';
  }

}


function mousePressed() {
  var posx = [50, 125, 200, 275, 350];
  var posy = 350;
  let x = mouseX;
  let y = mouseY;
  for (var i in posx) {
    if (x>posx[i] && x<posx[i]+50 && y>posy && y<posy+50) {
      console.log('Button pressed! ' + chips[i].value);
      if (bank - chips[i].value > -1 && lockBet == 'False') { //if bet does not cause a negative bank
        bet = bet + chips[i].value; //increase bet by chip value
        bank = bank - chips[i].value; //subtract bet from bank
        console.log('Bet: ' + bet);
        console.log('Bank: ' + bank);
        resetText = 'True';
      } else {
        console.log('Max bet!')
        resetText = 'True';
      }

    }
    posx2 = 275;
    posy2 = 225;
    //if (x>posx2[i] && x<posx2[i]+80 && y>posy2 && y<posy2+50) {
     // lockBet = 'True';
     // console.log('Bet placed!');
    }
  //}
}

function placeBet() {
  lockBet = 'True';
  console.log('Bet placed and locked!');
  resetText = 'True';
}

drawOne(newDeck, hand);
drawOne(newDeck, dealerHand);
drawOne(newDeck, hand);
drawOne(newDeck, dealerHand);

console.log("your hand: ");
console.log(hand);
console.log("dealer hand: ");
console.log(dealerHand);
sum = valueSum(hand, sum);
